﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DocAPi.Models
{


    public class ExSheet
    {
        public int ApplicationNo { get; set; }
        public string ApplicationDate { get; set; }
        public string PeriodTo { get; set; }
        public string ArchitechProjectNo { get; set; }
        public List<data> data { get; set; }

    }


    public class data
    {
        //public string cell { get; set; }
        //public string value { get; set; }
        //public string css { get; set; }
        //public string format { get; set; }
        public string ItemNo { get; set; }
        public string DescOfWork { get; set; }
        public string ScheduleValue { get; set; }
        public string FromPrvApp { get; set; }
        public string ThisPeriod { get; set; }
        public string MaterialPercentlyStored { get; set; }
        public string TotalComplitedStoredToDate { get; set; }
        public string PercentGC { get; set; }
        public string BalanceToFinish { get; set; }
        public string VRateRange { get; set; }
    }
}
