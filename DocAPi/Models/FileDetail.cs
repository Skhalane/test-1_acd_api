﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DocAPi.Models
{
    public class FileDetail
    {
        public string FileName { get; set; }
        public bool IsHtmlFileExist { get; set; }
    }
}
