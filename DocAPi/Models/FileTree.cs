﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DocAPi.Models
{
    public class FileTree
    {
        public string FileName { get; set; }
        public List<string> Version { get; set; }
    }
}
