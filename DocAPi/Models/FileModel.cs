﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DocAPi.Models
{
    public class FileModel
    {
        public string FileName { get; set; }
        public string FileCompletePath { get; set; }
        public string ParentFileName { get; set; }

    }
}
