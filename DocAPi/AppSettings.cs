﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DocAPi
{
    public class AppSettingsModel
    {
        public string SourceFolder { get; set; }

        public string ExcelFolder { get; set; }
        public string GemBoxDocument { get; set; }
        public string GemBoxSpreadsheet { get; set; }
            
        public string GemBoxLicenseKey { get; set; }
        public string JsonExcelFolder { get; set; }
    }
}
