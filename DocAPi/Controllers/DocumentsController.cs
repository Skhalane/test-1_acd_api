﻿using DocAPi.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using GemBox.Document;
using System.Configuration;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DocAPi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DocumentsController : ControllerBase
    {
        AppSettingsModel configuration = new AppSettingsModel();
        public DocumentsController(IOptions<AppSettingsModel> appSettings)
        {
            configuration = appSettings.Value;
        }

        [HttpGet]
        public async Task<List<FileTree>> Get()
        {
            List<FileTree> files = new List<FileTree>();

            string filename = string.Empty;
            ComponentInfo.SetLicense(configuration.GemBoxDocument);

            var sourcePath = configuration.SourceFolder;
            if (Directory.Exists(sourcePath))
            {
                var fileList = Directory.GetFiles(sourcePath, "*.*", SearchOption.AllDirectories)
                     .Where(a => new string[] { ".doc", ".docx" }
                     .Contains(Path.GetExtension(a))).Select(x => new FileInfo(x)).Where(f => (f.Attributes & FileAttributes.Hidden) == 0)
                     .OrderByDescending(x => x.LastWriteTime).GroupBy(a => a.Name).ToList();


                foreach (var item in fileList)
                {
                    FileTree file = new FileTree();
                    file.FileName = item.Key;
                    var folerpath = Path.Combine(sourcePath, file.FileName.Replace(".docx", "").Replace(".doc", ""));
                    if (Directory.Exists(folerpath))
                    {
                        var versionFileList = Directory.GetFiles(folerpath, "*.*", SearchOption.AllDirectories)
                    .Where(a => new string[] { ".html" }
                    .Contains(Path.GetExtension(a))).Select(x => new FileInfo(x)).Where(f => (f.Attributes & FileAttributes.Hidden) == 0)
                    .OrderByDescending(x => x.LastWriteTime).GroupBy(a => a.Name).ToList();
                        file.Version = versionFileList.Select(x => x.Key).ToList();
                    }
                    files.Add(file);
                }


            }
            return files;
        }

        [HttpPost("Save/{filename}")]
        public async Task<bool> Post(string filename, [FromBody] string htmlData)
        {
            ComponentInfo.SetLicense(configuration.GemBoxDocument);
            string folderName = filename.Split(".")[0];
            var sourcePath = configuration.SourceFolder;

            if (!Directory.Exists(Path.Combine(sourcePath, folderName)))
            {
                Directory.CreateDirectory(Path.Combine(sourcePath, folderName));
            }

            using (MemoryStream htmlMemoryStream = new MemoryStream())
            {
                htmlMemoryStream.Write(Encoding.ASCII.GetBytes(htmlData));

                var document = DocumentModel.Load(htmlMemoryStream, LoadOptions.HtmlDefault);

                var saveOptions = new HtmlSaveOptions()
                {
                    HtmlType = HtmlType.Html,
                    EmbedImages = true,
                    UseSemanticElements = false
                };
                var fileList = Directory.GetFiles(Path.Combine(sourcePath, folderName), "*.*", SearchOption.AllDirectories)
                  .Where(a => new string[] { ".html" }
                  .Contains(Path.GetExtension(a))).Select(x => new FileInfo(x)).Where(f => (f.Attributes & FileAttributes.Hidden) == 0)
                  .OrderByDescending(x => x.LastWriteTime).GroupBy(a => a.Name).ToList();
                string fileName = "Rev 1." + fileList.Count + ".html";
                // Save DocumentModel object to HTML (or MHTML) file.
                document.Save(Path.Combine(sourcePath, folderName, fileName), saveOptions);
            }



            return true;
        }

        [HttpGet("GetFileByName/{name}")]
        public async Task<FileDetail> GetFileByName(string name)
        {
            FileDetail fileDetail = new FileDetail();
            string relativeFilePath = string.Empty;
            ComponentInfo.SetLicense(configuration.GemBoxDocument);


            var sourcePath = configuration.SourceFolder;
            string htmlFilePath = name.Split(".")[0] + ".html";
            if (!System.IO.File.Exists(Path.Combine(sourcePath, htmlFilePath)))
            {
                fileDetail.IsHtmlFileExist = false;
                var document = DocumentModel.Load(sourcePath + name);
                var saveOptions = new HtmlSaveOptions()
                {
                    HtmlType = HtmlType.Html,
                    EmbedImages = true,
                    UseSemanticElements = true
                };

                // Save DocumentModel object to HTML (or MHTML) file.
                document.Save(sourcePath + htmlFilePath, saveOptions);
            }
            else { fileDetail.IsHtmlFileExist = true; }
            fileDetail.FileName = htmlFilePath;
            //relativeFilePath = htmlFilePath;
            return fileDetail;
        }


        public static readonly String Template = "DocumentPdf.docx";

        [HttpPost("Print/{filename}")]
        public async Task<FileDetail> PrintDocument(string filename, [FromBody] string htmlData)
        {


            FileDetail fileDetail = new FileDetail();
            ComponentInfo.SetLicense(configuration.GemBoxDocument);
            string folderName = filename.Split(".")[0];
            var sourcePath = configuration.SourceFolder;

            if (!Directory.Exists(Path.Combine(sourcePath, folderName)))
            {
                Directory.CreateDirectory(Path.Combine(sourcePath, folderName));
            }
            using (MemoryStream htmlMemoryStream = new MemoryStream())
            {
                string NewhtmlData = "<div><br/></div>" + htmlData;


                string[] RemoveData = { "[OtherExhibitsSustainabilityPlan]", "[OtherExhibitsE204]", "[OwnerRepName_1]", "[OwnerRepTitle]", "[ContractorRepName_1]", "[DigitalSignature1]", "[DigitalSignature2]", "[OtherDocuments]", "[SupplementaryTable]", "[OtherExhibitsSupplementaryConditions]", "[SustainabilityPlanTable]", " [SustainabilityPlanTable]", "[OtherExhibitsSustainabilityPlan] ", "[E204Date]", "[OtherExhibitsE204] ", "[AddendaTable]", "[SpecificationsTable]", "[DrawingsTable]", "[E203Date]", "[OtherProvisions]", "[InsuranceNotice]", "[ContractorRepName]", "[ContractorRepAddress]", "[ContractorRepTelephone]", "[ContractorRepFax]", "[ContractorRepMobile]", "[ContractorRepEmail]", "[OwnerRepName]", "[OwnerRepAddress]", "[OwnerRepTelephone]", "[OwnerRepFax]", "[OwnerRepMobile]", "[OwnerRepEmail]", "[Termination]", "[LitigationMethod]", "[OtherDisputeResolution]", "[SpecifiedDisputeResolution]", "[InitialDecisionMakerName]", "[InitialDecisionMakerAddress]", "[InitialDecisionMakerTelephone]", "[InitialDecisionMakerFax]", "[OverduePayIntRate]", "[BasisForInterest]", "[FinalPayTermsOrDate]", "[RetainageConditionsForRelease]", "[RetainageReductOrLimit]", "[RetainageExempt]", "[RetainagePerPayment]", "[DaysToPayContractor]", "[DaysToPayContractorWords]", "[MonthToPayContractor]", "[DOMToPayContractor]", "[DOMPayApplicationReceived]", "[ApplicationForPayPeriod]", "[OtherBonusProvisions]", "[LiquidatedDamagesEntireWork]", "[AllowancesTable]", "[AlternatesAdditionalTable]", "[AlternativesTable]", "[ContractSumWords]", "[CompletionOtherDate]", "[SubstantialCompletionOther]", "[SubstantialCompletionDaysWords]", "[SubstantialCompletionByDays]", "[CommencementOtherWords]", "[CommencementOther]", "[CommencementNoticeToProceed]", "[CommencementPerAgreement]", "[AgreementDateYearWords]", "[UnitPricesTable]", "[AcceptedAlternates]", "[ContractSum]", "[DamageOrBonusProvisions]", "[PortionOfWorkTable]", "[SubstantialCompletionDays]", "[OwnersTimeRequirement]", "[CommencementDate]", "[ContractorFax]", "[ContractorTelephone]", "[ContractorLongAddress]", "[ContractorLegalEntity]", "[ContractorFullFirmName]", "[ContractDateYearWords]", "[OwnerFullFirmName]", "[OwnerLegalEntity]", "[OwnerLongAddress]", "[OwnerTelephone]", "[OwnerFax]", "[ArchitectFullFirmName]", "[ArchitectLegalEntity]", "[ArchitectLongAddress]", "[ArchitectTelephone]", "[ArchitectFax]", "[ProjectName]", "[ProjectLocation]", "[ProjectDescription]", "[InitialInformation]", "[AdditionalServices]", "[SiteVisitsWords]", "[SiteVisits]", "[CompletionOfServicesWords]", "[ArbitrationMethod]" };


                List<string> list = new List<string>();
                list.AddRange(RemoveData);
                foreach (string i in list)
                    NewhtmlData = NewhtmlData.Replace(i, "");

                htmlMemoryStream.Write(Encoding.ASCII.GetBytes(NewhtmlData));

                 var document = DocumentModel.Load(htmlMemoryStream, LoadOptions.HtmlDefault);
                string extension = Path.GetExtension(filename);

                var saveOptions = new HtmlSaveOptions()
                {
                    HtmlType = HtmlType.Html,
                    EmbedImages = true,
                    UseSemanticElements = false
                };


                var fileList = Directory.GetFiles(Path.Combine(sourcePath, folderName), "*.*", SearchOption.AllDirectories)
                  .Where(a => new string[] { ".html" }
                  .Contains(Path.GetExtension(a))).Select(x => new FileInfo(x)).Where(f => (f.Attributes & FileAttributes.Hidden) == 0)
                  .OrderByDescending(x => x.LastWriteTime).GroupBy(a => a.Name).ToList();


                string fileName = "Rev 1." + fileList.Count + ".html";
                //// Save DocumentModel object to HTML (or MHTML) file.
                document.Save(Path.Combine(sourcePath, folderName, fileName), saveOptions);


                //PDF CODE
                try
                {


                    string filePathWithoutExt = filename.Substring(0, filename.Length - extension.Length);
                    DateTime Todate = DateTime.Now;
                    Section section = document.Sections[0];
                    PageSetup pageSetup = section.PageSetup;
                    PageMargins pageMargins = pageSetup.PageMargins;
                    pageMargins.Top = pageMargins.Bottom = pageMargins.Left = pageMargins.Right = 40;

                    Picture Leftadata = new Picture(document, "LeftData.png");
                    Picture pic1 = new Picture(document, "Watermark.png");
                    Picture pic3 = new Picture(document, "headerImage.png", 100, 40, LengthUnit.Pixel);
                    Picture Linepic = new Picture(document, "line.png");


                    FloatingLayout layout3 = new FloatingLayout(new HorizontalPosition(5.8, LengthUnit.Inch, HorizontalPositionAnchor.Page), new VerticalPosition(2.8, LengthUnit.Inch, VerticalPositionAnchor.Page), pic1.Layout.Size);
                    layout3.WrappingStyle = TextWrappingStyle.BehindText;

                    FloatingLayout layout2 = new FloatingLayout(new HorizontalPosition(5.8, LengthUnit.Inch, HorizontalPositionAnchor.Page), new VerticalPosition(2.8, LengthUnit.Inch, VerticalPositionAnchor.Page), Leftadata.Layout.Size);
                    layout3.WrappingStyle = TextWrappingStyle.InFrontOfText;



                    FloatingLayout layout1 = new FloatingLayout(new HorizontalPosition(1, LengthUnit.Inch, HorizontalPositionAnchor.Page), new VerticalPosition(0.9, LengthUnit.Inch, VerticalPositionAnchor.Page), pic3.Layout.Size);
                    layout1.WrappingStyle = TextWrappingStyle.BehindText;


                    FloatingLayout layout = new FloatingLayout(new HorizontalPosition(0.6, LengthUnit.Inch, HorizontalPositionAnchor.Page), new VerticalPosition(10.2, LengthUnit.Inch, VerticalPositionAnchor.Page), Linepic.Layout.Size);

                    layout3.WrappingStyle = TextWrappingStyle.BehindText;
                    pic1.Layout = layout3;
                    Leftadata.Layout = layout2;
                    pic3.Layout = layout1;
                    Linepic.Layout = layout;


                    //section.Blocks[2].Content.Start.InsertRange(new Paragraph(document, Leftadata).Content);


                    section.Blocks[2].Content.Start.InsertRange(new Paragraph(document, pic3).Content);

                    section.Blocks[0].Content.LoadText("<p style='font-size: 25px;text-align: center'>AIA Document " + filePathWithoutExt + "</p>", new HtmlLoadOptions());

                    section.Blocks[1].Content.LoadText("<p style='font-size: 20px;padding-left:80px;font-style: italic;'>Standard Form of Agreement Between Owner and Architect</p>", new HtmlLoadOptions());



                    section.HeadersFooters.Add(new HeaderFooter(document, HeaderFooterType.FooterDefault,
                    new Paragraph(document, Linepic), new Paragraph(document, pic1), new Paragraph(document, new Run(document, "AIA Document  " + filePathWithoutExt + ". Copyright © 1974, 1978, 1987, 1997, 2007 and 2017 by The American Institute of Architects. All rights reserved.")
                    {
                        CharacterFormat = { Size = 8 }

                    }, new Run(document, "American Institute of Architects,” “AIA,” the AIA Logo, and “AIA Contract Documents” are registered trademarks and may not be used without permission") { CharacterFormat = { Size = 8, FontColor = Color.Red } }, new Run(document, ". This draft was produced by AIA software at " + Todate + " under Order No.8515036444 which expires on 11/30/2021, is not for resale, is licensed for one-time use only, and may only be used in accordance with the AIA Contract Documents® Terms of Service. To report copyright violations, e-mail copyright@aia.org.User Notes")
                    { CharacterFormat = { Size = 8 } }, new Field(document, FieldType.Page), new Run(document, " of "), new Field(document, FieldType.NumPages))));



                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }


                //End code



                fileName = "Rev 1." + fileList.Count + ".pdf";
                document.Save(Path.Combine(sourcePath, folderName, fileName));
                fileDetail.FileName = Path.Combine(folderName, fileName);
                fileDetail.IsHtmlFileExist = true;
                return fileDetail;



            }

        }




        //[HttpPost("Print/{filename}")]
        //public async Task<ActionResult> PrintDocument(string filename, [FromBody] string htmlData)
        //{
        //    ComponentInfo.SetLicense(configuration.GemBoxDocument);
        //    string folderName = filename.Split(".")[0];
        //    var sourcePath = configuration.SourceFolder;

        //    if (!Directory.Exists(Path.Combine(sourcePath, folderName)))
        //    {
        //        Directory.CreateDirectory(Path.Combine(sourcePath, folderName));
        //    }

        //    using (MemoryStream htmlMemoryStream = new MemoryStream())
        //    {
        //        htmlMemoryStream.Write(Encoding.ASCII.GetBytes(htmlData));

        //        var document = DocumentModel.Load(htmlMemoryStream, LoadOptions.HtmlDefault);

        //        var saveOptions = new HtmlSaveOptions()
        //        {
        //            HtmlType = HtmlType.Html,
        //            EmbedImages = true,
        //            UseSemanticElements = false
        //        };
        //        var fileList = Directory.GetFiles(Path.Combine(sourcePath, folderName), "*.*", SearchOption.AllDirectories)
        //          .Where(a => new string[] { ".html" }
        //          .Contains(Path.GetExtension(a))).Select(x => new FileInfo(x)).Where(f => (f.Attributes & FileAttributes.Hidden) == 0)
        //          .OrderByDescending(x => x.LastWriteTime).GroupBy(a => a.Name).ToList();
        //        string fileName = "Rev 1." + fileList.Count + ".html";
        //        // Save DocumentModel object to HTML (or MHTML) file.
        //        document.Save(Path.Combine(sourcePath, folderName, fileName), saveOptions);

        //        //Added by Anurag
        //        string content = System.IO.File.ReadAllText(Path.Combine(sourcePath, folderName, fileName));
        //        fileName = "Rev 1." + fileList.Count + ".pdf";
        //        document.Save(Path.Combine(sourcePath, folderName, fileName));

        //        DocumentModel document2 = DocumentModel.Load(Template);
        //        foreach (Section section in document2.Sections)
        //        {
        //            PageSetup pageSetup = section.PageSetup;
        //            pageSetup.Orientation = Orientation.Portrait;
        //            pageSetup.PaperType = PaperType.Letter;

        //        }

        //        document2.DefaultCharacterFormat.Size = 8;

        //        string[] ContentsToRemove = { "[OwnerFullFirmName]", "[OwnerLegalEntity]", "[OwnerLongAddress]", "[OwnerTelephone]", "[OwnerFax]", "[ArchitectFullFirmName]", "[ArchitectLegalEntity]", "[", "]" };
        //        // Create a List and add a collection  
        //        List<string> OwnerDetails = new List<string>();
        //        OwnerDetails.AddRange(ContentsToRemove);
        //        foreach (string a in OwnerDetails)
        //            htmlData = htmlData.Replace(a, " ");

        //        //var document1 = DocumentModel.Load(SRC, LoadOptions.HtmlDefault);
        //        document2.Content.Replace("A141TM – 2014", Path.GetFileNameWithoutExtension(filename));


        //        DateTime dateTime = DateTime.Today;
        //        string date = dateTime.ToString("dd/MM/yyyy");
        //        DateTime lastYear = DateTime.Today.AddYears(1);
        //        string date1 = lastYear.ToString("dd/MM/yyyy");
        //        string Time = DateTime.Now.ToString("HH:mm:ss tt");
        //        string year = Convert.ToString(dateTime.Year);

        //        document2.Content.Replace("A141TM – 2014", "Anurag");
        //        document2.Content.Replace("2014", Convert.ToString(dateTime.Year));
        //        document2.Content.Replace("12:15:45", DateTime.Now.ToString("HH:mm:ss tt"));
        //        document2.Content.Replace("01/08/2018", dateTime.ToString("dd/MM/yyyy"));
        //        document2.Content.Replace("01/08/2019", lastYear.ToString("dd/MM/yyyy"));


        //        // Insert HTML formatted text after the previous text.
        //        var position = document2.Content.Start.LoadText("<p><br/><br/></p>",
        //                         LoadOptions.HtmlDefault);
        //        position.LoadText(htmlData,
        //            LoadOptions.HtmlDefault);


        //        document2.Save(@"C:\Doc\GeneratedPDF.pdf");

        //        string filePath = Path.Combine(sourcePath, folderName, fileName);
        //        var fileByte = System.IO.File.ReadAllBytes(filePath);
        //        return new FileContentResult(fileByte, "application/pdf");

        //    }

        //}
    }
}
