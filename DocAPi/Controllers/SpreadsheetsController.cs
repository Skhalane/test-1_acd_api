﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DocAPi.Models;
using GemBox.Document;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using GemBox.Document;
using GemBox.Document.Tables;
using GemBox.Spreadsheet;
using System.Text.RegularExpressions;
using System.Globalization;

namespace DocAPi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SpreadsheetsController : ControllerBase
    {
        AppSettingsModel configuration = new AppSettingsModel();
        public SpreadsheetsController(IOptions<AppSettingsModel> appSettings)
        {
            configuration = appSettings.Value;
        }

        [HttpGet]
        public async Task<List<FileTree>> Get()
        {

            List<FileTree> files = new List<FileTree>();
            var ExcelPath = configuration.ExcelFolder;
            if (Directory.Exists(ExcelPath))
            {
                var fileList = Directory.GetFiles(ExcelPath, "*.*", SearchOption.AllDirectories)
                   .Where(a => new string[] { ".xls", ".xlsx" }
                   .Contains(Path.GetExtension(a))).Select(x => new FileInfo(x)).Where(f => (f.Attributes & FileAttributes.Hidden) == 0)
                   .OrderByDescending(x => x.LastWriteTime).GroupBy(a => a.Name).ToList();


                foreach (var item in fileList)
                {
                    FileTree file = new FileTree();
                    file.FileName = item.Key;
                    file.Version = new List<string>();
                    var folerpath = Path.Combine(ExcelPath, file.FileName.Replace(".xlsx", "").Replace(".xls", ""));
                    if (Directory.Exists(folerpath))
                    {
                        var versionFileList = Directory.GetFiles(folerpath, "*.*", SearchOption.AllDirectories)
                    .Where(a => new string[] { ".json" }
                    .Contains(Path.GetExtension(a))).Select(x => new FileInfo(x)).Where(f => (f.Attributes & FileAttributes.Hidden) == 0)
                    .OrderByDescending(x => x.LastWriteTime).GroupBy(a => a.Name).ToList();
                        file.Version = versionFileList.Select(x => x.Key).ToList();
                    }
                    files.Add(file);
                }

            }


            return files;
        }

        [HttpGet("GetFileByName/{name}")]
        //[HttpGet("GetExcelData")]t
        public async Task<ActionResult> GetFileByName(string name)
        {
            try
            {
                FileDetail fileDetail = new FileDetail();
                string relativeFilePath = string.Empty;
                ComponentInfo.SetLicense(configuration.GemBoxDocument);
                //// If using Professional version, put your serial key below.
                SpreadsheetInfo.SetLicense(configuration.GemBoxSpreadsheet);

                var sourcePath = configuration.ExcelFolder;
                string htmlFilePath = name.Split(".")[0] + ".xlsx";
                string path = Path.Combine(sourcePath, htmlFilePath);


                // Load Excel workbook from file's path.
                ExcelFile workbook = ExcelFile.Load(path);
                // Iterate through all worksheets in a workbook.
                List<data> details = new List<data>();
                sheets sheet = new sheets();
                ExSheet Ext = new ExSheet();
                foreach (ExcelWorksheet worksheet in workbook.Worksheets)
                {
                    // Display sheet's name.
                    //Console.WriteLine("{1} {0} {1}\n", worksheet.Name, new string('#', 30));
                    if (worksheet.Index == 0)
                    {
                        sheet.name = Convert.ToString(worksheet.Name);
                    }

                    string[] Names = { "itemNo", "descOfWork", "scheduleValue", "fromPrvApp", "thisPeriod", "materialPercentlyStored", "totalComplitedStoredToDate", "percentGC", "balanceToFinish", "rateRange" };
                    // Iterate through all rows in a worksheet.
                    foreach (ExcelRow row in worksheet.Rows)
                    {
                        if (row.Index == 2)
                        {
                            foreach (ExcelCell cell in row.AllocatedCells)
                            {
                                if (cell.Name == "I3")
                                {
                                    Ext.ApplicationNo = Convert.ToInt32(cell.Value);
                                    break;
                                }
                            }
                        }

                        if (row.Index == 3)
                        {
                            foreach (ExcelCell cell in row.AllocatedCells)
                            {
                                if (cell.Name == "I4")
                                {
                                    Ext.ApplicationDate = Convert.ToString(cell.Value);
                                    break;
                                }
                            }
                        }
                        if (row.Index == 4)
                        {
                            foreach (ExcelCell cell in row.AllocatedCells)
                            {
                                if (cell.Name == "I5")
                                {
                                    Ext.PeriodTo = Convert.ToString(cell.Value);
                                    break;
                                }
                            }
                        }
                        if (row.Index == 5)
                        {
                            foreach (ExcelCell cell in row.AllocatedCells)
                            {
                                if (cell.Name == "I6")
                                {
                                    Ext.ArchitechProjectNo = Convert.ToString(cell.Value);
                                    break;
                                }
                            }
                        }



                        if (row.Index > 8)
                        {
                            string nm = string.Empty;
                            string bm = string.Empty;
                            string def = string.Empty;
                            string CName = string.Empty;
                            string DName = string.Empty;
                            string EName = string.Empty;
                            string FName = string.Empty;
                            string GName = string.Empty;
                            string HName = string.Empty;
                            string IName = string.Empty;
                            string JName = string.Empty;

                            // Iterate through all allocated cells in a row.
                            foreach (ExcelCell cell in row.AllocatedCells)
                            {

                                //if (cell.Value != "" && cell.Value != null )
                                //{
                                switch (cell.Column.Name)
                                {
                                    case "A":
                                        Names[0] = Convert.ToString(cell.Value);
                                        break;
                                    case "B":
                                        Names[1] = Convert.ToString(cell.Value);
                                        break;
                                    case "C":
                                        Names[2] = Convert.ToString(cell.Value == null ? 0 : cell.Value);
                                        CName = Convert.ToString(cell.Column) + Convert.ToString(cell.Row.Index);
                                        break;
                                    case "D":
                                        Names[3] = Convert.ToString(cell.Value == null ? 0 : cell.Value);
                                        DName = Convert.ToString(cell.Column) + Convert.ToString(cell.Row.Index);
                                        break;
                                    case "E":
                                        Names[4] = Convert.ToString(cell.Value == null ? 0 : cell.Value);
                                        EName = Convert.ToString(cell.Column) + Convert.ToString(cell.Row.Index);
                                        break;
                                    case "F":
                                        Names[5] = Convert.ToString(cell.Value == null ? 0 : cell.Value);
                                        FName = Convert.ToString(cell.Column) + Convert.ToString(cell.Row.Index);
                                        break;
                                    case "G":
                                        Names[6] = Convert.ToString(cell.Value == null ? 0 : cell.Value);
                                        GName = Convert.ToString(cell.Column) + Convert.ToString(cell.Row.Index);
                                        break;
                                    case "H":
                                        Names[7] = Convert.ToString(cell.Value == null ? 0 : cell.Value);
                                        HName = Convert.ToString(cell.Column) + Convert.ToString(cell.Row.Index);
                                        break;
                                    case "I":
                                        Names[8] = Convert.ToString(cell.Value == null ? 0 : cell.Value);
                                        IName = Convert.ToString(cell.Column) + Convert.ToString(cell.Row.Index);
                                        break;
                                    case "J":
                                        Names[9] = Convert.ToString(cell.Value == null ? 0 : cell.Value);
                                        JName = IName = Convert.ToString(cell.Column) + Convert.ToString(cell.Row.Index);
                                        if (Names[1] == "GRAND TOTAL")
                                        {
                                            details.Add(new data { ItemNo = Names[0], DescOfWork = Convert.ToString(Names[1]), ScheduleValue = "=Sum(C10:" + Convert.ToString(CName) + ")", FromPrvApp = "=Sum(D10:" + Convert.ToString(DName) + ")", ThisPeriod = "=Sum(E10:" + Convert.ToString(EName) + ")", MaterialPercentlyStored = "=Sum(F10:" + Convert.ToString(FName) + ")", TotalComplitedStoredToDate = "=Sum(G10:" + Convert.ToString(GName) + ")", PercentGC = "=IF(C" + cell.Row.Name + " = 0,0,(G" + cell.Row.Name + "/C" + cell.Row.Name + "))", BalanceToFinish = "=Sum(I10:" + Convert.ToString(IName) + ")", VRateRange = "=Sum(J10:" + Convert.ToString(JName) + ")" });
                                        }
                                        else
                                        {
                                            details.Add(new data { ItemNo = Names[0], DescOfWork = Convert.ToString(Names[1]), ScheduleValue = Convert.ToString(Names[2]), FromPrvApp = Convert.ToString(Names[3]), ThisPeriod = Convert.ToString(Names[4]), MaterialPercentlyStored = Convert.ToString(Names[5]), VRateRange = Convert.ToString(Names[9]), TotalComplitedStoredToDate = "=D" + cell.Row.Name + "+E" + cell.Row.Name + "+F" + cell.Row.Name + "", PercentGC = "=IF(C" + cell.Row.Name + " = 0,0,(G" + cell.Row.Name + "/C" + cell.Row.Name + "))", BalanceToFinish = "=C" + cell.Row.Name + "-G" + cell.Row.Name + "" });
                                        }
                                        break;
                                }

                            }
                        }
                    }
                }
                Ext.data = details;

                //return Ok(details);
                return Ok(Ext);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [HttpPost("Save/{filename}")]
        public async Task<bool> Post(string filename, [FromBody] string excelDatas)
        {
            try
            {
                excelDatas = excelDatas.Replace('"', ' ');
                excelDatas = excelDatas.Replace("'", "\"");
                excelDatas = excelDatas.Replace('\\', '"');
                excelDatas = excelDatas.Replace("\"r", "");
                excelDatas = excelDatas.Replace("null", "");
                excelDatas = excelDatas.Replace("\"n", "");
                //dynamic json = JsonConvert.DeserializeObject(excelDatas);
                List<ExcelDataModel> JsonData = JsonConvert.DeserializeObject<List<ExcelDataModel>>(excelDatas);
                foreach (var item in JsonData)
                {
                    if (item.Cell == "I3")
                    {
                        if (item.Value == "undefined")
                        {
                            item.Value = "001";
                        }

                    }
                    if (item.Cell == "I4")
                    {
                        if (item.Value == "undefined")
                        {
                            item.Value = Convert.ToString(DateTime.Now);
                        }
                    }
                    if (item.Cell == "I5")
                    {
                        if (item.Value == "undefined")
                        {
                            item.Value = Convert.ToString(DateTime.Now);
                        }
                    }
                    if (item.Cell == "I6")
                    {
                        if (item.Value == "undefined")
                        {
                            item.Value = "G703";
                        }
                    }
                }

                JsonData.RemoveAll(kk => kk.Value == "undefined");
                data dt = null;
                //string[] Names = { "itemNo", "descOfWork", "scheduleValue", "fromPrvApp", "thisPeriod", "materialPercentlyStored", "totalComplitedStoredToDate", "percentGC", "balanceToFinish", "rateRange" };
                int i = 0;
                var list = new List<string[]>();
                ExSheet Ext = new ExSheet();
                Ext.ApplicationNo = Convert.ToInt32(JsonData[4].Value);
                Ext.ApplicationDate = Convert.ToString(JsonData[6].Value); //Convert.ToString(DateTime.Now);//Convert.ToDateTime(JsonData[6].Value);
                Ext.PeriodTo = Convert.ToString(JsonData[8].Value);//Convert.ToString(DateTime.Now); //Convert.ToDateTime(JsonData[8].Value);
                Ext.ArchitechProjectNo = Convert.ToString(JsonData[10].Value);

                data dta = new data();
                List<data> lst = new List<data>();
                int cellval = 0;
                //foreach (var item in JsonData.Skip(90))
                string[] Names = { "itemNo", "descOfWork", "scheduleValue", "fromPrvApp", "thisPeriod", "materialPercentlyStored", "totalComplitedStoredToDate", "percentGC", "balanceToFinish", "rateRange" };
                foreach (var item in JsonData.Skip(32))
                {
                    i++;
                    //string na = Names[i];
                    //string Values = item.Value;

                    switch (i)
                    {
                        case 1:
                            dta.ItemNo = Convert.ToString(item.Value);
                            break;
                        case 2:
                            dta.DescOfWork = Convert.ToString(item.Value);
                            break;
                        case 3:
                            dta.ScheduleValue = Convert.ToString(item.Value);
                            break;
                        case 4:
                            dta.FromPrvApp = Convert.ToString(item.Value);
                            break;
                        case 5:
                            dta.ThisPeriod = Convert.ToString(item.Value);
                            break;
                        case 6:
                            dta.MaterialPercentlyStored = Convert.ToString(item.Value);
                            break;
                        case 7:
                            cellval = Convert.ToInt32(Regex.Match(item.Cell, @"\d+").Value);
                            dta.TotalComplitedStoredToDate = Convert.ToString("=D" + cellval + "+E" + cellval + "+F" + cellval + "");
                            break;
                        case 8:
                            cellval = Convert.ToInt32(Regex.Match(item.Cell, @"\d+").Value);
                            dta.PercentGC = Convert.ToString("=IF(C" + cellval + " = 0,0,(G" + cellval + "/C" + cellval + "))");
                            break;
                        case 9:
                            cellval = Convert.ToInt32(Regex.Match(item.Cell, @"\d+").Value);
                            dta.BalanceToFinish = Convert.ToString("=C" + cellval + "-G" + cellval + "");
                            break;
                        case 10:
                            dta.VRateRange = Convert.ToString(item.Value);
                            lst.Add(dta);
                            dta = new data();
                            i = 0;
                            break;

                    }
                }
                Ext.data = lst;
                //var ad = lst.SkipLast(1).ToList();
                string sJSONResponse = JsonConvert.SerializeObject(Ext);
                //string sJSONResponse = JsonConvert.SerializeObject(lst);
                string folderName = filename.Split(".")[0];
                var sourcePath = configuration.ExcelFolder;

                if (!Directory.Exists(Path.Combine(sourcePath, folderName)))
                {
                    Directory.CreateDirectory(Path.Combine(sourcePath, folderName));
                }
                var fileList = Directory.GetFiles(Path.Combine(sourcePath, folderName), "*.*", SearchOption.AllDirectories)
                 .Where(a => new string[] { ".json" }
                 .Contains(Path.GetExtension(a))).Select(x => new FileInfo(x)).Where(f => (f.Attributes & FileAttributes.Hidden) == 0)
                 .OrderByDescending(x => x.LastWriteTime).GroupBy(a => a.Name).ToList();
                string fileName = "Rev 1." + fileList.Count + ".json";
                string paths = Path.Combine(sourcePath, folderName, fileName);
                //open file stream
                using (StreamWriter file = System.IO.File.CreateText(Path.Combine(sourcePath, folderName, fileName)))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    //serialize object directly into file stream
                    //serializer.Serialize(file, lst);
                    serializer.Serialize(file, Ext);
                }



            }
            catch (Exception ex)
            {

            }
            return true;
        }




        [HttpPost("Print/{filename}")]
        public async Task<FileDetail> PrintDocuments(string filename, [FromBody] string excelDatas)
        {
            try
            {

                SpreadsheetInfo.SetLicense(configuration.GemBoxSpreadsheet);
                FileDetail fileDetail = new FileDetail();
                string folderName = filename.Split(".")[0];
                var sourcePath = configuration.ExcelFolder;


                if (!Directory.Exists(Path.Combine(sourcePath, folderName)))
                {
                    Directory.CreateDirectory(Path.Combine(sourcePath, folderName));
                }


                ExcelFile workbook = ExcelFile.Load("SpreadSheetPrint.xlsx");

                //foreach (ExcelWorksheet worksheet in workbook.Worksheets)
                //{
                //    ExcelPrintOptions sheetPrintOptions = worksheet.PrintOptions;

                //    sheetPrintOptions.Portrait = false;
                //    sheetPrintOptions.HorizontalCentered = true;
                //    sheetPrintOptions.VerticalCentered = true;

                //    sheetPrintOptions.PrintHeadings = true;
                //    sheetPrintOptions.PrintGridlines = true;
                //}


                excelDatas = excelDatas.Replace('"', ' ');
                excelDatas = excelDatas.Replace("'", "\"");
                excelDatas = excelDatas.Replace('\\', '"');
                excelDatas = excelDatas.Replace("\"r", "");
                excelDatas = excelDatas.Replace("null", "");
                excelDatas = excelDatas.Replace("\"n", "");
                //dynamic json = JsonConvert.DeserializeObject(excelDatas);
                List<ExcelDataModel> lst = JsonConvert.DeserializeObject<List<ExcelDataModel>>(excelDatas);
                lst.RemoveAll(kk => kk.Value == "undefined");
                var worksheet = workbook.Worksheets[0];
                ExcelPrintOptions sheetPrintOptions = worksheet.PrintOptions;
                sheetPrintOptions.Portrait = false;
                sheetPrintOptions.HorizontalCentered = true;
                sheetPrintOptions.VerticalCentered = true;
                //sheetPrintOptions.PrintHeadings = true;
                //sheetPrintOptions.PrintGridlines = true;

                foreach (var item in lst)
                {
                    if (item.Cell == "I3")
                    {
                        worksheet.Cells[item.Cell].Value = item.Value;
                    }
                    if (item.Cell == "I4")
                    {
                        worksheet.Cells[item.Cell].Value = item.Value;
                    }
                    if (item.Cell == "I5")
                    {
                        worksheet.Cells[item.Cell].Value = item.Value;
                    }
                    if (item.Cell == "I6")
                    {
                        worksheet.Cells[item.Cell].Value = item.Value;
                    }

                }

                int i = 0;
                int Gt = 0;
                int value;
                decimal decVal;
                string v;
                string[] CNames = { "B260", "C260", "D260", "E260", "F260", "G260", "H260", "I260", "J260" };
                foreach (var item in lst.Skip(32))
                {
                    item.Value = item.Value.Replace(",", "");
                    if (item.Value == "GRAND TOTAL")
                    {
                        Gt = 1;

                    }
                    if (Gt == 1)
                    {
                        string CellName = string.Empty;
                        if (item.Value == "GRAND TOTAL") //till grand total 
                        {
                            CellName = CNames[i];
                            worksheet.Cells[CNames[i]].Value = item.Value;
                        }
                        else
                        {

                            CellName = CNames[i];
                            string str2 = Regex.Replace(CellName, @"[\d-]", string.Empty);
                            if (str2 == "H")
                            {
                                decimal val = Convert.ToDecimal(item.Value) * 100;
                                worksheet.Cells[CNames[i]].Value = Math.Round(Convert.ToDecimal(val), 2, MidpointRounding.AwayFromZero) + "%";
                            }
                            else
                            {

                                if (Convert.ToDecimal(item.Value) < 0)
                                {
                                    decimal res = Convert.ToDecimal(item.Value);
                                    res = res * (-1);
                                    if (int.TryParse(item.Value, out value))
                                    {   
                                        worksheet.Cells[CNames[i]].Value = "-$" + Math.Round(Convert.ToDecimal(res), 2) + ".00";
                                    }
                                    else
                                    {
                                        worksheet.Cells[CNames[i]].Value = "-$" + Math.Round(Convert.ToDecimal(res), 2);
                                    }

                                }
                                else
                                {

                                    if (int.TryParse(item.Value, out value))
                                    {
                                        v = Convert.ToString(value) + ".00";
                                        string kks =  Convert.ToDecimal(v).ToString("C", System.Globalization.CultureInfo.GetCultureInfo("en-us"));
                                        worksheet.Cells[CNames[i]].Value = kks;//"$" + Convert.ToDecimal(v);
                                    }
                                    else
                                    {
                                        worksheet.Cells[CNames[i]].Value = Convert.ToDecimal(item.Value).ToString("C", System.Globalization.CultureInfo.GetCultureInfo("en-us"));//"$" + Math.Round(Convert.ToDecimal(item.Value),2);
                                    }

                                }


                            }
                        }
                        i++;
                    }
                    else if (Gt == 0)
                    {
                        if (item.Value == "0")
                        {
                            item.Value = "0.00";
                        }
                        var str = Regex.Replace(item.Cell, @"[\d-]", string.Empty); // Remove num from string
                        if (str == "H")
                        {
                            decimal val = Convert.ToDecimal(item.Value) * 100;
                            worksheet.Cells[item.Cell].Value = Math.Round(Convert.ToDecimal(val), 2, MidpointRounding.AwayFromZero) + "%";
                        }
                        else
                        {
                            //var str1 = Regex.IsMatch(item.Value, @"^\d+$");
                            if (Regex.IsMatch(item.Value, @"^\d+$"))
                            {
                                if (int.TryParse(item.Value, out value))
                                {
                                    v = Convert.ToString(value) + ".00";
                                    worksheet.Cells[item.Cell].Value = Math.Round(Convert.ToDecimal(v), 2);
                                }
                                else
                                {
                                    worksheet.Cells[item.Cell].Value = Math.Round(Convert.ToDecimal(item.Value), 2);//item.Value;
                                }
                            }
                            else
                            {
                                if (Decimal.TryParse(item.Value, out decVal))
                                {
                                    worksheet.Cells[item.Cell].Value = Math.Round(Convert.ToDecimal(item.Value), 2); //Convert.ToString(Math.Round(Convert.ToDecimal(item.Value), 2));
                                }
                                else
                                {
                                    worksheet.Cells[item.Cell].Value = item.Value;
                                }

                                    
                            }

                        }

                    }
                }



                string fileName = "Rev 1." + ".pdf";

                workbook.Save(Path.Combine(sourcePath, folderName, fileName), new GemBox.Spreadsheet.PdfSaveOptions() { SelectionType = SelectionType.EntireFile });
                fileDetail.FileName = Path.Combine(folderName, fileName);
                fileDetail.IsHtmlFileExist = true;



                return fileDetail;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }



}
