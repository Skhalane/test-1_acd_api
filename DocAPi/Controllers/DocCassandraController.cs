﻿using Cassandra;
using Cassandra.Data.Linq;
using DocAPi.Models;
using GemBox.Document;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocAPi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DocCassandraController : ControllerBase
    {
        AppSettingsModel configuration = new AppSettingsModel();
        public DocCassandraController(IOptions<AppSettingsModel> appSettings)
        {
            configuration = appSettings.Value;
        }

        public class Doc_Files_name
        {
            public int id { get; set; }
            public string doc_data { get; set; }
            public string filename { get; set; }

            public string fileversion { get; set; }
            public string template { get; set; }


        }

        public class docs_files
        {
            public Guid id { get; set; }
            public string doc_data { get; set; }
            public string filename { get; set; }

            public string fileversion { get; set; }
            public string template { get; set; }


        }

        public class docs_files2
        {
            public Guid id { get; set; }
            public string filename { get; set; }

            public string fileversion { get; set; }
            public string template { get; set; }


        }

        public class docArray
        {
            public string filename { get; set; }

            public List<Version> Version { get; set; }

        }
        public class Version
        {
            public string FileVersion { get; set; }
        }





        [HttpGet("GetDocList")]
        public async Task<ActionResult> Get()
        {
            try
            {



                var cluster = Cluster.Builder().AddContactPoint("127.0.0.1").Build();
                var session = cluster.Connect("acd_documentkeyspace");

                var results1 = session.GetTable<docs_files2>("docs_files").Execute();



                var table = from p in results1
                            group p by p.filename into g
                            select new { filename = g.Key, Version = g.ToList() };





                return Ok(table);


            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return null;
        }



        [HttpGet("GetFileByName/{filename}")]
        public async Task<ActionResult> GetFileByName(string filename)
        {
            try
            {

                List<Doc_Files_name> lstFile = new List<Doc_Files_name>();

                List<docs_files> lstFileDetails = new List<docs_files>();

                var cluster = Cluster.Builder().AddContactPoint("127.0.0.1").Build();
                var session = cluster.Connect("acd_documentkeyspace");



                var results = session.Execute("SELECT * FROM docs_files WHERE filename = '" + filename + "' allow filtering ; ");

                Doc_Files_name obj = null;
                foreach (var result in results)
                {

                    obj = new Doc_Files_name();
                    obj.filename = result.GetValue<string>("filename");


                    obj.doc_data = result.GetValue<string>("doc_data");



                    lstFile.Add(obj);
                }

                return Ok(lstFile);


            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return null;
        }

        [HttpPost("SaveVersion/{filename}")]
        public async Task<ActionResult> SaveVersion(string filename, [FromBody] string htmlData)
        {


            try
            {

                string[] RemoveData = { "[__________________]" };
                List<string> list = new List<string>();
                list.AddRange(RemoveData);
                foreach (string i in list)
                    htmlData = htmlData.Replace(i, "");

                List<Doc_Files_name> lstFile = new List<Doc_Files_name>();

                List<docs_files> lstFileDetails = new List<docs_files>();
                var cluster = Cluster.Builder().AddContactPoint("127.0.0.1").Build();
                var session = cluster.Connect("acd_documentkeyspace");
                string Template = System.IO.Path.GetFileNameWithoutExtension(filename);


                var results1 = session.GetTable<docs_files>("docs_files").Execute();
                var data = results1.Where(u => u.filename == filename).Count();
                string VfileName = "Rev_" + data;


                var results = session.Execute("insert into docs_files(id,doc_data,filename,fileversion,template)values(uuid(),'" + htmlData + "','" + filename + "','" + VfileName + "','" + Template + "')");

                Doc_Files_name obj = null;
                foreach (var result in results)
                {

                    obj = new Doc_Files_name();
                    obj.doc_data = result.GetValue<string>("doc_data");
                    obj.filename = result.GetValue<string>("filename");
                    obj.fileversion = result.GetValue<string>("fileversion");
                    obj.template = result.GetValue<string>("template");
                    lstFile.Add(obj);
                }


                return Ok(lstFile);


                // Guid myuuid = Guid.NewGuid();


            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }



            return null;
        }



        [HttpGet("GetVersionFileByName/{Versionfilename}")]
        public async Task<ActionResult> GetVersionFileByName(string Versionfilename)
        {
            try
            {
                var cluster = Cluster.Builder().AddContactPoint("127.0.0.1").Build();
                var session = cluster.Connect("acd_documentkeyspace");

                var results1 = session.GetTable<docs_files>("docs_files").Execute();
                var data = results1.Where(u => u.fileversion == Versionfilename);

                return Ok(data);


            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return null;
        }



        [HttpGet("GetVersionCount/{filename}")]
        public async Task<ActionResult> GetVersionCount(string filename)
        {
            try
            {
                var cluster = Cluster.Builder().AddContactPoint("127.0.0.1").Build();
                var session = cluster.Connect("acd_documentkeyspace");

                var results1 = session.GetTable<docs_files>("docs_files").Execute();
                var data = results1.Where(u => u.filename == filename).Count();

                return Ok(data);


            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return null;
        }



        [HttpPost("Print/{filename}")]
        public async Task<FileDetail> PrintDocument(string filename, [FromBody] string htmlData)
        {



            FileDetail fileDetail = new FileDetail();
            ComponentInfo.SetLicense(configuration.GemBoxDocument);
            string folderName = filename.Split(".")[0];
            var sourcePath = configuration.SourceFolder;

            if (!Directory.Exists(Path.Combine(sourcePath, folderName)))
            {
                Directory.CreateDirectory(Path.Combine(sourcePath, folderName));
            }
            using (MemoryStream htmlMemoryStream = new MemoryStream())
            {
                string NewhtmlData = "<div><br><br><br><br></div>" + htmlData;

                string[] RemoveData = { "[__________________]", "[AgreementDateYearWords]", "[UnitPricesTable]", "[AcceptedAlternates]", "[ContractSum]", "[DamageOrBonusProvisions]", "[PortionOfWorkTable]", "[SubstantialCompletionDays]", "[OwnersTimeRequirement]", "[CommencementDate]", "[ContractorFax]", "[ContractorTelephone]", "[ContractorLongAddress]", "[ContractorLegalEntity]", "[ContractorFullFirmName]", "[ContractDateYearWords]", "[OwnerFullFirmName]", "[OwnerLegalEntity]", "[OwnerLongAddress]", "[OwnerTelephone]", "[OwnerFax]", "[ArchitectFullFirmName]", "[ArchitectLegalEntity]", "[ArchitectLongAddress]", "[ArchitectTelephone]", "[ArchitectFax]", "[ProjectName]", "[ProjectLocation]", "[ProjectDescription]", "[InitialInformation]", "[AdditionalServices]", "[SiteVisitsWords]", "[SiteVisits]", "[CompletionOfServicesWords]", "[ArbitrationMethod]" };
                List<string> list = new List<string>();
                list.AddRange(RemoveData);
                foreach (string i in list)
                    NewhtmlData = NewhtmlData.Replace(i, "");

                htmlMemoryStream.Write(Encoding.ASCII.GetBytes(NewhtmlData));

                var document = DocumentModel.Load(htmlMemoryStream, LoadOptions.HtmlDefault);
                string extension = Path.GetExtension(filename);

                var saveOptions = new HtmlSaveOptions()
                {
                    HtmlType = HtmlType.Html,
                    EmbedImages = true,
                    UseSemanticElements = false
                };


                var fileList = Directory.GetFiles(Path.Combine(sourcePath, folderName), "*.*", SearchOption.AllDirectories)
                  .Where(a => new string[] { ".html" }
                  .Contains(Path.GetExtension(a))).Select(x => new FileInfo(x)).Where(f => (f.Attributes & FileAttributes.Hidden) == 0)
                  .OrderByDescending(x => x.LastWriteTime).GroupBy(a => a.Name).ToList();


                string fileName = "Rev 1." + fileList.Count + ".html";
                // Save DocumentModel object to HTML (or MHTML) file.
                document.Save(Path.Combine(sourcePath, folderName, fileName), saveOptions);


                //PDF CODE
                try
                {


                    string filePathWithoutExt = filename.Substring(0, filename.Length - extension.Length);
                    DateTime Todate = DateTime.Now;
                    Section section = document.Sections[0];
                    PageSetup pageSetup = section.PageSetup;
                    PageMargins pageMargins = pageSetup.PageMargins;
                    pageMargins.Top = pageMargins.Bottom = pageMargins.Left = pageMargins.Right = 40;

                    Picture picture2 = new Picture(document, "LeftData.png");
                    Picture pic1 = new Picture(document, "Watermark.png");
                    Picture pic3 = new Picture(document, "headerImage.png", 100, 40, LengthUnit.Pixel);
                    Picture Linepic = new Picture(document, "line.png");



                    FloatingLayout layout3 = new FloatingLayout(new HorizontalPosition(5.8, LengthUnit.Inch, HorizontalPositionAnchor.Page), new VerticalPosition(2.8, LengthUnit.Inch, VerticalPositionAnchor.Page), picture2.Layout.Size);
                    layout3.WrappingStyle = TextWrappingStyle.BehindText;

                    FloatingLayout layout2 = new FloatingLayout(new HorizontalPosition(5.8, LengthUnit.Inch, HorizontalPositionAnchor.Page), new VerticalPosition(2.8, LengthUnit.Inch, VerticalPositionAnchor.Page), picture2.Layout.Size);
                    layout3.WrappingStyle = TextWrappingStyle.InFrontOfText;




                    FloatingLayout layout1 = new FloatingLayout(new HorizontalPosition(1, LengthUnit.Inch, HorizontalPositionAnchor.Page), new VerticalPosition(0.9, LengthUnit.Inch, VerticalPositionAnchor.Page), pic3.Layout.Size);
                    layout1.WrappingStyle = TextWrappingStyle.BehindText;


                    FloatingLayout layout = new FloatingLayout(new HorizontalPosition(0.6, LengthUnit.Inch, HorizontalPositionAnchor.Page), new VerticalPosition(10.2, LengthUnit.Inch, VerticalPositionAnchor.Page), Linepic.Layout.Size);

                    layout3.WrappingStyle = TextWrappingStyle.BehindText;
                    pic1.Layout = layout3;
                    picture2.Layout = layout2;
                    pic3.Layout = layout1;
                    Linepic.Layout = layout;



                    section.Blocks[2].Content.Start.InsertRange(new Paragraph(document, picture2).Content);


                    section.Blocks[3].Content.Start.InsertRange(new Paragraph(document, pic3).Content);

                    section.Blocks[0].Content.LoadText("<p style='font-size: 25px;text-align: center'>AIA Document " + filePathWithoutExt + "</p>", new HtmlLoadOptions());

                    section.Blocks[1].Content.LoadText("<p style='font-size: 20px;padding-left:80px;font-style: italic;'>Standard Form of Agreement Between Owner and Architect</p>", new HtmlLoadOptions());




                    section.HeadersFooters.Add(new HeaderFooter(document, HeaderFooterType.FooterDefault,
                    new Paragraph(document, Linepic), new Paragraph(document, pic1), new Paragraph(document, new Run(document, "AIA Document  " + filePathWithoutExt + ". Copyright © 1974, 1978, 1987, 1997, 2007 and 2017 by The American Institute of Architects. All rights reserved.")
                    {
                        CharacterFormat = { Size = 8 }

                    }, new Run(document, "American Institute of Architects,” “AIA,” the AIA Logo, and “AIA Contract Documents” are registered trademarks and may not be used without permission") { CharacterFormat = { Size = 8, FontColor = Color.Red } }, new Run(document, ". This draft was produced by AIA software at " + Todate + " under Order No.8515036444 which expires on 11/30/2021, is not for resale, is licensed for one-time use only, and may only be used in accordance with the AIA Contract Documents® Terms of Service. To report copyright violations, e-mail copyright@aia.org.User Notes")
                    { CharacterFormat = { Size = 8 } }, new Field(document, FieldType.Page), new Run(document, " of "), new Field(document, FieldType.NumPages))));





                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }



                //End code


                fileName = "Rev 1." + fileList.Count + ".pdf";
                document.Save(Path.Combine(sourcePath, folderName, fileName));
                fileDetail.FileName = Path.Combine(folderName, fileName);
                fileDetail.IsHtmlFileExist = true;
                return fileDetail;




            }

        }


    }





}
