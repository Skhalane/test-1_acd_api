﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DocAPi.Models;
using GemBox.Document;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using GemBox.Document;
using GemBox.Document.Tables;
using GemBox.Spreadsheet;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Dynamic;
using Newtonsoft.Json.Converters;

namespace DocAPi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SpreadSheetWController : ControllerBase
    {
        AppSettingsModel configuration = new AppSettingsModel();
        public SpreadSheetWController(IOptions<AppSettingsModel> appSettings)
        {
            configuration = appSettings.Value;
        }

        [HttpGet]
        public async Task<List<FileTree>> Get()
        {

            List<FileTree> files = new List<FileTree>();
            var ExcelPath = configuration.JsonExcelFolder;
            if (Directory.Exists(ExcelPath))
            {
                var fileList = Directory.GetFiles(ExcelPath, "*.*", SearchOption.AllDirectories)
                   .Where(a => new string[] { ".txt", ".txt" }
                   .Contains(Path.GetExtension(a))).Select(x => new FileInfo(x)).Where(f => (f.Attributes & FileAttributes.Hidden) == 0)
                   .OrderByDescending(x => x.LastWriteTime).GroupBy(a => a.Name).ToList();


                foreach (var item in fileList)
                {
                    FileTree file = new FileTree();
                    file.FileName = item.Key;
                    file.Version = new List<string>();
                    var folerpath = Path.Combine(ExcelPath, file.FileName.Replace(".txt", "").Replace(".txt", ""));
                    if (Directory.Exists(folerpath))
                    {
                        var versionFileList = Directory.GetFiles(folerpath, "*.*", SearchOption.AllDirectories)
                    .Where(a => new string[] { ".json" }
                    .Contains(Path.GetExtension(a))).Select(x => new FileInfo(x)).Where(f => (f.Attributes & FileAttributes.Hidden) == 0)
                    .OrderByDescending(x => x.LastWriteTime).GroupBy(a => a.Name).ToList();
                        file.Version = versionFileList.Select(x => x.Key).ToList();
                    }
                    files.Add(file);
                }

            }


            return files;
        }

        [HttpGet("GetFileByName/{name}")]
        //[HttpGet("GetExcelData")]t
        public async Task<ActionResult> GetFileByName(string name)
        {
            try
            {
                var sourcePath = configuration.JsonExcelFolder;
                string htmlFilePath = name.Split(".")[0] + ".txt";
                string path = Path.Combine(sourcePath, htmlFilePath);

                //var sourcePath = configuration.ExcelFolder;
                string jsonString1 = System.Text.Json.JsonSerializer.Serialize("workbook.json");
                //var workbookdata = Path.Combine(sourcePath, "ExcelJson","workbook.json");
                using (StreamReader r = new StreamReader(path))
                {
                    var json = r.ReadToEnd();

                    JObject json1 = JObject.Parse(json);
                    dynamic jsonData = JsonConvert.DeserializeObject<dynamic>(json);
                    //dynamic config = JsonConvert.DeserializeObject<ExpandoObject>(json, new ExpandoObjectConverter());
                    var json12 = JsonConvert.SerializeObject(jsonData);
                    string j = json12;
                    return Ok(j);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [HttpPost("Save/{filename}")]
        public async Task<bool> Post(string filename, [FromBody] string excelDatas)
        {
            try
            {
                //string filename = "G703.xlsx";
                string sJSONResponse = JsonConvert.SerializeObject(excelDatas);
                //string sJSONResponse = JsonConvert.SerializeObject(lst);
                string folderName = filename.Split(".")[0];
                var sourcePath = configuration.JsonExcelFolder;
                dynamic json123 = JsonConvert.DeserializeObject(excelDatas);
                if (!Directory.Exists(Path.Combine(sourcePath, folderName)))
                {
                    Directory.CreateDirectory(Path.Combine(sourcePath, folderName));
                }
                var fileList = Directory.GetFiles(Path.Combine(sourcePath, folderName), "*.*", SearchOption.AllDirectories)
                 .Where(a => new string[] { ".json" }
                 .Contains(Path.GetExtension(a))).Select(x => new FileInfo(x)).Where(f => (f.Attributes & FileAttributes.Hidden) == 0)
                 .OrderByDescending(x => x.LastWriteTime).GroupBy(a => a.Name).ToList();
                string fileName = "Rev 1." + fileList.Count + ".json";
                string paths = Path.Combine(sourcePath, folderName, fileName);
                //open file stream
                using (StreamWriter file = System.IO.File.CreateText(Path.Combine(sourcePath, folderName, fileName)))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    //serialize object directly into file stream
                    //serializer.Serialize(file, lst);
                    serializer.Serialize(file, json123);
                }

            }
            catch (Exception ex)
            {

            }
            return true;
        }


        [HttpGet("LoadData")]
        public ActionResult LoadWorkbookData()
        {

            using (StreamReader r = new StreamReader("file.json"))
            {
                string json = r.ReadToEnd();
                //List<Item> items = JsonConvert.DeserializeObject<List<Item>>(json);
            }
            return Ok();
        }


        [HttpPost("sav")]
        public ActionResult Save([FromBody] string excelDatas)
        {
            string filename = "G703.xlsx";
            string sJSONResponse = JsonConvert.SerializeObject(excelDatas);
            //string sJSONResponse = JsonConvert.SerializeObject(lst);
            string folderName = filename.Split(".")[0];
            var sourcePath = configuration.ExcelFolder;
            dynamic json123 = JsonConvert.DeserializeObject(excelDatas);
            if (!Directory.Exists(Path.Combine(sourcePath, folderName)))
            {
                Directory.CreateDirectory(Path.Combine(sourcePath, folderName));
            }
            var fileList = Directory.GetFiles(Path.Combine(sourcePath, folderName), "*.*", SearchOption.AllDirectories)
             .Where(a => new string[] { ".json" }
             .Contains(Path.GetExtension(a))).Select(x => new FileInfo(x)).Where(f => (f.Attributes & FileAttributes.Hidden) == 0)
             .OrderByDescending(x => x.LastWriteTime).GroupBy(a => a.Name).ToList();
            string fileName = "Rev 1." + fileList.Count + ".json";
            string paths = Path.Combine(sourcePath, folderName, fileName);
            //open file stream
            using (StreamWriter file = System.IO.File.CreateText(Path.Combine(sourcePath, folderName, fileName)))
            {
                JsonSerializer serializer = new JsonSerializer();
                //serialize object directly into file stream
                //serializer.Serialize(file, lst);
                serializer.Serialize(file, json123);
            }
            return Ok();

        }

        [HttpPost("load")]
        public ActionResult load([FromBody] string excelDatas)
        {
            //string filename = "G703.xlsx";
            //string sJSONResponse = JsonConvert.SerializeObject(excelDatas);
            ////string sJSONResponse = JsonConvert.SerializeObject(lst);
            //string folderName = filename.Split(".")[0];
            //var sourcePath = configuration.ExcelFolder;
            //dynamic json123 = JsonConvert.DeserializeObject(excelDatas);
            //if (!Directory.Exists(Path.Combine(sourcePath, folderName)))
            //{
            //    Directory.CreateDirectory(Path.Combine(sourcePath, folderName));
            //}
            //var fileList = Directory.GetFiles(Path.Combine(sourcePath, folderName), "*.*", SearchOption.AllDirectories)
            // .Where(a => new string[] { ".json" }
            // .Contains(Path.GetExtension(a))).Select(x => new FileInfo(x)).Where(f => (f.Attributes & FileAttributes.Hidden) == 0)
            // .OrderByDescending(x => x.LastWriteTime).GroupBy(a => a.Name).ToList();
            //string fileName = "Rev 1." + fileList.Count + ".json";
            //string paths = Path.Combine(sourcePath, folderName, fileName);
            ////open file stream
            //using (StreamWriter file = System.IO.File.CreateText(Path.Combine(sourcePath, folderName, fileName)))
            //{
            //    JsonSerializer serializer = new JsonSerializer();
            //    //serialize object directly into file stream
            //    //serializer.Serialize(file, lst);
            //    serializer.Serialize(file, json123);
            //}
            //return Ok();


            //var sourcePath = configuration.ExcelFolder;
            string jsonString1 = System.Text.Json.JsonSerializer.Serialize("workbook.json");
            //var workbookdata = Path.Combine(sourcePath, "ExcelJson","workbook.json");
            using (StreamReader r = new StreamReader("workbook.json"))
            {
                var json = r.ReadToEnd();

                JObject json1 = JObject.Parse(json);
                dynamic jsonData = JsonConvert.DeserializeObject<dynamic>(json);

                dynamic config = JsonConvert.DeserializeObject<ExpandoObject>(json, new ExpandoObjectConverter());

                //var abcc = jsonData.jsonObject.Workbook.sheets[0].rows[8].cells[0].value;
                //jsonData.jsonObject.Workbook.sheets[0].rows[8].Remove();//.cells[0].value = "Name";
                //foreach (var key in new string[] { "cell" })
                //{
                //    foreach (var item in jsonData.jsonObject.Workbook.sheets[0].rows[8])
                //    {
                //        item.Remove();
                //    }

                //}

                //int i = 0;
                //foreach (var item in jsonData)
                //{
                //    var kks = item.ChildrenTokens;
                //    //  var abcs = item.First[i];
                //    //dynamic jsonData1 = JsonConvert.DeserializeObject<dynamic>(item);
                //    //var kks = abcs.value;
                //    //foreach (var item1 in item)
                //    //{
                //    //    var abcs = item1.Last;
                //    //    var kks = abcs.value;
                //    //}
                //    i++;
                //}



                var jobject = JsonConvert.DeserializeObject<JObject>(json);
                var abc = jobject.First;
                var dff = abc.First;
                var kkj = dff.First;
                var kkd = kkj.First;
                var sheets = kkd.Last;
                var tsd = kkd.Last;
                var tsdds = tsd.Last;
                object jsonString = System.Text.Json.JsonSerializer.Serialize<string>(json);
                object deserializedProduct = JsonConvert.DeserializeObject<object>(json);
                var json12 = JsonConvert.SerializeObject(jsonData);
                string j = json12;
                return Ok(j);
            }


            //return Ok();
        }


        [HttpPost("Print/{filename}")]
        public async Task<FileDetail> PrintDocuments(string filename, [FromBody] string excelDatas)
        {
            try
            {

                SpreadsheetInfo.SetLicense(configuration.GemBoxSpreadsheet);
                FileDetail fileDetail = new FileDetail();
                string folderName = filename.Split(".")[0];
                var sourcePath = configuration.ExcelFolder;


                if (!Directory.Exists(Path.Combine(sourcePath, folderName)))
                {
                    Directory.CreateDirectory(Path.Combine(sourcePath, folderName));
                }




                //foreach (ExcelWorksheet worksheet in workbook.Worksheets)
                //{
                //    ExcelPrintOptions sheetPrintOptions = worksheet.PrintOptions;

                //    sheetPrintOptions.Portrait = false;
                //    sheetPrintOptions.HorizontalCentered = true;
                //    sheetPrintOptions.VerticalCentered = true;

                //    sheetPrintOptions.PrintHeadings = true;
                //    sheetPrintOptions.PrintGridlines = true;
                //}


                excelDatas = excelDatas.Replace('"', ' ');
                excelDatas = excelDatas.Replace("'", "\"");
                excelDatas = excelDatas.Replace('\\', '"');
                excelDatas = excelDatas.Replace("\"r", "");
                excelDatas = excelDatas.Replace("null", "");
                excelDatas = excelDatas.Replace("\"n", "");
                //dynamic json = JsonConvert.DeserializeObject(excelDatas);
                List<ExcelDataModel> lst = JsonConvert.DeserializeObject<List<ExcelDataModel>>(excelDatas);
                ExcelFile workbook = null;
                if (lst.Count > 1950)
                {
                    workbook = ExcelFile.Load("SpreadSheetPrint55.xlsx");
                }
                else
                {
                    workbook = ExcelFile.Load("SpreadSheetPrint.xlsx");
                }

                var I3 = lst.Where(a => a.Cell == "I3").Select(k => k.Value).FirstOrDefault();
                if (Convert.ToString(I3) == "undefined")
                {
                    lst.Where(w => w.Cell == "I3").ToList().ForEach(s => s.Value = "");
                }
                var I4 = lst.Where(a => a.Cell == "I4").Select(k => k.Value).FirstOrDefault();
                if (Convert.ToString(I4) == "undefined")
                {
                    lst.Where(w => w.Cell == "I4").ToList().ForEach(s => s.Value = "");
                }
                var I5 = lst.Where(a => a.Cell == "I5").Select(k => k.Value).FirstOrDefault();
                if (Convert.ToString(I5) == "undefined")
                {
                    lst.Where(w => w.Cell == "I5").ToList().ForEach(s => s.Value = "");
                }
                var I6 = lst.Where(a => a.Cell == "I6").Select(k => k.Value).FirstOrDefault();
                if (Convert.ToString(I6) == "undefined")
                {
                    lst.Where(w => w.Cell == "I6").ToList().ForEach(s => s.Value = "");
                }
                lst.RemoveAll(kk => kk.Value == "undefined");
                var worksheet = workbook.Worksheets[0];
                ExcelPrintOptions sheetPrintOptions = worksheet.PrintOptions;
                sheetPrintOptions.Portrait = false;
                sheetPrintOptions.HorizontalCentered = true;
                sheetPrintOptions.VerticalCentered = true;
                //sheetPrintOptions.PrintHeadings = true;
                //sheetPrintOptions.PrintGridlines = true;

                foreach (var item in lst)
                {
                    if (item.Cell == "I3")
                    {
                        worksheet.Cells[item.Cell].Value = item.Value;
                    }
                    if (item.Cell == "I4")
                    {
                        worksheet.Cells[item.Cell].Value = item.Value;
                    }
                    if (item.Cell == "I5")
                    {
                        worksheet.Cells[item.Cell].Value = item.Value;
                    }
                    if (item.Cell == "I6")
                    {
                        worksheet.Cells[item.Cell].Value = item.Value;
                    }

                }

                int i = 0;
                int Gt = 0;
                int value;
                decimal decVal;
                string v;
                string[] CNames = { "B260", "C260", "D260", "E260", "F260", "G260", "H260", "I260", "J260" };
                foreach (var item in lst.Skip(22))
                {
                    item.Value = item.Value.Replace(",", "");
                    if (item.Value == "GRAND TOTAL")
                    {
                        Gt = 1;

                    }
                    if (Gt == 1)
                    {
                        string CellName = string.Empty;
                        if (item.Value == "GRAND TOTAL") //till grand total 
                        {
                            CellName = CNames[i];
                            worksheet.Cells[CNames[i]].Value = item.Value;
                        }
                        else
                        {

                            CellName = CNames[i];
                            string str2 = Regex.Replace(CellName, @"[\d-]", string.Empty);
                            if (str2 == "H")
                            {
                                decimal val = Convert.ToDecimal(item.Value) * 100;
                                worksheet.Cells[CNames[i]].Value = Math.Round(Convert.ToDecimal(val), 2, MidpointRounding.AwayFromZero) + "%";
                            }
                            else
                            {

                                if (Convert.ToDecimal(item.Value) < 0)
                                {
                                    decimal res = Convert.ToDecimal(item.Value);
                                    res = res * (-1);
                                    if (int.TryParse(item.Value, out value))
                                    {
                                        worksheet.Cells[CNames[i]].Value = "-$" + Math.Round(Convert.ToDecimal(res), 2) + ".00";
                                    }
                                    else
                                    {
                                        worksheet.Cells[CNames[i]].Value = "-$" + Math.Round(Convert.ToDecimal(res), 2);
                                    }

                                }
                                else
                                {

                                    if (int.TryParse(item.Value, out value))
                                    {
                                        v = Convert.ToString(value) + ".00";
                                        string kks = Convert.ToDecimal(v).ToString("C", System.Globalization.CultureInfo.GetCultureInfo("en-us"));
                                        worksheet.Cells[CNames[i]].Value = kks;//"$" + Convert.ToDecimal(v);
                                        if (CNames[i] == "J260")
                                        {
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        worksheet.Cells[CNames[i]].Value = Convert.ToDecimal(item.Value).ToString("C", System.Globalization.CultureInfo.GetCultureInfo("en-us"));//"$" + Math.Round(Convert.ToDecimal(item.Value),2);
                                    }

                                }


                            }
                        }
                        i++;
                    }
                    else if (Gt == 0)
                    {
                        if (item.Value == "0")
                        {
                            item.Value = "0.00";
                        }
                        var str = Regex.Replace(item.Cell, @"[\d-]", string.Empty); // Remove num from string
                        if (str == "H")
                        {
                            decimal val = Convert.ToDecimal(item.Value) * 100;
                            worksheet.Cells[item.Cell].Value = Math.Round(Convert.ToDecimal(val), 2, MidpointRounding.AwayFromZero) + "%";
                        }
                        else
                        {
                            //var str1 = Regex.IsMatch(item.Value, @"^\d+$");
                            if (Regex.IsMatch(item.Value, @"^\d+$"))
                            {
                                if (int.TryParse(item.Value, out value))
                                {
                                    v = Convert.ToString(value) + ".00";
                                    worksheet.Cells[item.Cell].Value = Math.Round(Convert.ToDecimal(v), 2);
                                }
                                else
                                {
                                    worksheet.Cells[item.Cell].Value = Math.Round(Convert.ToDecimal(item.Value), 2);//item.Value;
                                }
                            }
                            else
                            {
                                if (Decimal.TryParse(item.Value, out decVal))
                                {
                                    worksheet.Cells[item.Cell].Value = Math.Round(Convert.ToDecimal(item.Value), 2); //Convert.ToString(Math.Round(Convert.ToDecimal(item.Value), 2));
                                }
                                else
                                {
                                    worksheet.Cells[item.Cell].Value = item.Value;
                                }


                            }

                        }

                    }
                }



                string fileName = "Rev 1." + ".pdf";

                workbook.Save(Path.Combine(sourcePath, folderName, fileName), new GemBox.Spreadsheet.PdfSaveOptions() { SelectionType = SelectionType.EntireFile });
                fileDetail.FileName = Path.Combine(folderName, fileName);
                fileDetail.IsHtmlFileExist = true;



                return fileDetail;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}